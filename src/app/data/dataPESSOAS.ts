/* eslint-disable @typescript-eslint/naming-convention */

/* tslint:disable */
export const PessoasData = [{
  Id: 0,
  Name: 'Paulo Victor de Jesus Marques',
  Points: 100943,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 1,
  Name: 'Abel Brun',
  Points: 39315,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 2,
  Name: 'Keira Walker',
  Points: 34116,
  Ispessoa: true,
  Posicao:0

},
{
  Id: 4,
  Name: 'Moritz Braun',
  Points: 48081,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 5,
  Name: 'Roberto Carlos',
  Points: 14109,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 6,
  Name: 'Loinrope',
  Points: 3902,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 7,
  Name: 'Osxea',
  Points: 2423,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 8,
  Name: 'Gipal',
  Points: 6113,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 9,
  Name: 'Isgir',
  Points: 1072,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 10,
  Name: 'Pueon',
  Points: 5118,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 11,
  Name: 'Luizinho',
  Points: 4798,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 12,
  Name: 'Leandro Yasuzawa',
  Points: 5489,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 13,
  Name: 'Raul Pena',
  Points: 9071,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 14,
  Name: 'Matheus Simoes Pinsdorf',
  Points: 0,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 15,
  Name: 'Marcia Alves Simeoes',
  Points: 7000,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 16,
  Name: 'Joseana Jesus Marques',
  Points: 8090,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 17,
  Name: 'MAX',
  Points: 7894,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 18,
  Name: 'PINS',
  Points: 7544,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 19,
  Name: 'LOLA',
  Points: 6549,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 20,
  Name: 'Claudinho',
  Points: 3102,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 21,
  Name: 'Roberto Tsuneki',
  Points: 10,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 22,
  Name: 'Italo Ferreira',
  Points: 1798,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 23,
  Name: 'Larissa Ribas',
  Points: 1638,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 24,
  Name: 'Lucas Sakamoto',
  Points: 5467,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 25,
  Name: 'Bruno Cilento',
  Points: 3654,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 26,
  Name: 'Ricardo Ibra',
  Points: 36662,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 27,
  Name: 'Naruto Usumaki',
  Points: 650,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 28,
  Name: 'Jonathan Caleri',
  Points: 80564,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 29,
  Name: 'Ricardo Volpi',
  Points: 65800,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 30,
  Name: 'Cassio',
  Points: 78941,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 31,
  Name: 'Reinaldo',
  Points: 79415,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 32,
  Name: 'Luan Petrolhead',
  Points: 55000,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 33,
  Name: 'Mirellinha',
  Points: 60000,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 34,
  Name: 'Izabela',
  Points: 44701,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 35,
  Name: 'Carla Peres',
  Points: 16257,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 36,
  Name: 'Samantha',
  Points: 53189,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 37,
  Name: 'Heloisa',
  Points: 56189,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 38,
  Name: 'Eduardo Razuk',
  Points: 43597,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 39,
  Name: 'Lucas Fontana',
  Points: 79455,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 40,
  Name: 'Felipe Auto-Super',
  Points: 63862,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 41,
  Name: 'Felipe Retondar',
  Points: 82716,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 42,
  Name: 'Cooker',
  Points: 45548,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 43,
  Name: 'Miguel',
  Points: 75308,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 44,
  Name: 'Miguelito',
  Points: 70268,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 45,
  Name: 'Gaston',
  Points: 38506,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 46,
  Name: 'Basille',
  Points: 57201,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 47,
  Name: 'Jamil',
  Points: 20749,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 48,
  Name: 'Mia',
  Points: 18296,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 49,
  Name: 'Douglas',
  Points: 48149,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 50,
  Name: 'Luis Careca',
  Points: 51036,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 51,
  Name: 'Vinicius Armesto',
  Points: 80326,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 52,
  Name: 'Elian Garcia',
  Points: 26744,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 53,
  Name: 'Nadia Lascala',
  Points: 88690,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 55,
  Name: 'Enzo Lascala',
  Points: 18300,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 55,
  Name: 'Alzira',
  Points: 96253,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 56,
  Name: 'Augusto Pererinha',
  Points: 33788,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 57,
  Name: 'Maria Luiza',
  Points: 93907,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 58,
  Name: 'Muricy Ramalho',
  Points: 59632,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 59,
  Name: 'Pai do Paulinho',
  Points: 55451,
  Ispessoa: true,
  Posicao:0
},
{
  Id: 60,
  Name: 'Muie',
  Points: 14109,
  Ispessoa: true,
  Posicao:0
}
]

import { ModalConfirmacaoComponent } from './../modal-confirmacao/modal-confirmacao.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-modal-trabalho-voluntario',
  templateUrl: './modal-trabalho-voluntario.component.html',
  styleUrls: ['./modal-trabalho-voluntario.component.scss']
})
export class ModalTrabalhoVoluntarioComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar ,public dialogRef: MatDialogRef<ModalTrabalhoVoluntarioComponent>,public dialog: MatDialog) { }
  ShowSnackBar(message: string){
    this._snackBar.open(message, 'Ok');
    this.dialogRef.close();
    const dialogRef = this.dialog.open(ModalConfirmacaoComponent);

  }

  ngOnInit(): void {
  }

}

import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import {
	IgxAvatarModule,
	IgxBadgeModule,
	IgxButtonModule,
	IgxGridModule,
	IgxIconModule,
	IgxInputGroupModule,
	IgxProgressBarModule,
	IgxRippleModule,
	IgxSwitchModule
 } from "igniteui-angular";

import { GridPessoasComponent } from './grids/grid-pessoas/grid-pessoas.component';
import { TelaPrincipalComponent } from './ui/tela-principal/tela-principal.component';
import { ToolbarSuperiorComponent } from './ui/toolbar-superior/toolbar-superior.component';
import { LoginComponent } from './ui/modal/login/login.component';
import { CadastroComponent } from './ui/cadastro/cadastro.component';
import { PerfilOngComponent } from './ui/perfil-ong/perfil-ong.component';
import { PerfilUserComponent } from './ui/perfil-user/perfil-user.component';
import { MatDividerModule }  from '@angular/material/divider'
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCommonModule, MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import { GridOngsComponent } from './grids/grid-ongs/grid-ongs.component';
import { RatingModule } from 'ng-starrating';
import { ModalBoasVindasComponent } from './ui/modal/modal-boas-vindas/modal-boas-vindas.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { PaginaNaoEncontradaComponent } from './ui/pagina-nao-encontrada/pagina-nao-encontrada.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatRadioModule} from '@angular/material/radio';
import {MatGridListModule} from '@angular/material/grid-list';
import { ModalTrabalhoVoluntarioComponent } from './ui/modal/modal-trabalho-voluntario/modal-trabalho-voluntario.component';
import { ModalDoacaoComponent } from './ui/modal/modal-doacao/modal-doacao.component';
import { ModalConfirmacaoComponent } from './ui/modal/modal-confirmacao/modal-confirmacao.component';



@NgModule({
  bootstrap: [AppComponent],
  declarations: [
	AppComponent,
	GridPessoasComponent,
	TelaPrincipalComponent,
	ToolbarSuperiorComponent,
	LoginComponent,
	CadastroComponent,
	PerfilOngComponent,
	PerfilUserComponent,
	GridOngsComponent,
	ModalBoasVindasComponent,
	PaginaNaoEncontradaComponent,
	ModalTrabalhoVoluntarioComponent,
	ModalDoacaoComponent,
	ModalConfirmacaoComponent
],
  imports: [
	BrowserModule,
	BrowserAnimationsModule,
	FormsModule,
	IgxAvatarModule,
	IgxBadgeModule,
	IgxButtonModule,
	IgxGridModule,
	IgxIconModule,
	IgxInputGroupModule,
	IgxProgressBarModule,
	IgxRippleModule,
	IgxSwitchModule,
	HttpClientModule,
  MatDividerModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatCommonModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTableModule,
  MatPaginatorModule,
  MatSelectModule,
  RatingModule,
  MatDialogModule,
  MatSnackBarModule,
  MatMenuModule,
  RouterModule,
  MatRadioModule,
  MatGridListModule,
  RouterModule.forRoot([
    {path: 'home', component: TelaPrincipalComponent},
    {path: 'ong/:nome', component: PerfilOngComponent},
    {path: 'perfil/:nome', component: PerfilUserComponent},
    {path: 'cadastro', component: CadastroComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    { path: '**', component: PaginaNaoEncontradaComponent },
  ]),
],
  providers: [],
  entryComponents: [],
  schemas: []
})
export class AppModule {}

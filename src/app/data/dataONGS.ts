/* eslint-disable @typescript-eslint/naming-convention */

/* tslint:disable */
export const ONGSData = [{
  Id: 1,
  Avatar: 'assets/ongs/AmigosDoBem.png',
  Name: 'Amigos do Bem',
  Nivel_Carencia: 1 ,
  Descricao: "Transformamos Vidas Por Meio De Educação, Geração De Renda E De Diversos Projetos. Sua ajuda nos ajuda a transformar a sociedade a se tornar mais justa e igualitária. Doe como puder. Responsabilidade Social. Ajude a transformar vidas.",
  Area: 'Comunidades Carentes',
  Site: 'https://www.amigosdobem.org/'
},
{
  Id: 2,
  Avatar: 'assets/ongs/SOSMataAtlantica.jpg',
  Name: 'SOS Mata Atlantica',
  Nivel_Carencia: 5,
  Descricao: "A Fundação SOS Mata Atlântica foi criada em 1986 como uma organização não governamental e sem fins lucrativos, [1] com o objetivo de defender o que resta de Mata Atlântica no Brasil. [2] Suas ações estão divididas em seis eixos: políticas públicas; campanhas; documentação, informação e comunicação para a conservação; educação ambiental e boa cidadania; desenvolvimento institucional; e desenvolvimento sustentável, proteção e manejo de ecossistemas.",
  Area: 'Meio Ambiente',
  Site: 'https://www.sosma.org.br/'
},
{
  Id: 3,
  Avatar: 'assets/ongs/medicos_sem_fronteiras.png',
  Name: 'Medicos Sem Fronteiras',
  Nivel_Carencia: 2,
  Descricao: "Médicos Sem Fronteiras (MSF) é uma organização humanitária internacional que leva cuidados de saúde a pessoas afetadas por graves crises humanitárias. Também é missão de MSF chamar a atenção para as dificuldades enfrentadas pelos pacientes atendidos em seus projetos.",
  Area: 'Medica',
  Site: 'https://www.msf.org.br/'
}
,
{
  Id: 4,
  Avatar: 'assets/ongs/Aliados.png',
  Name: 'Movimento Aliados',
  Nivel_Carencia: 1,
  Descricao: "Temos como objetivo oferecer toda a assistência de valorização dos Direitos Humanos da população LGBT, por meio de ações e projetos que estimulem o respeito à existência da diversidade sexual, acompanhando e implementando todas as ações pertinentes para esse fim. Visão: Tornar-se referência de movimento quanto às questões ligados ao universo LGBT com vistas a a visibilidade e inclusão dessa população em todos os setores sociais. Valores: Honestidade, coragem, transparência, valorização da vida, espírito de equipe, responsabilidade social, ética, comprometimento.",
  Area: 'LGBTQIAP+',
  Site: 'https://www.facebook.com/movimentoaliados/'
},
{
  Id: 5,
  Avatar: 'assets/ongs/todxs.jpg',
  Name: 'Todxs',
  Nivel_Carencia: 3,
  Descricao: "Somos uma organização sem fins lucrativos que promove a inclusão de pessoas LGBTI+ na sociedade com iniciativas de formação de lideranças, pesquisa, conscientização e segurança.",
  Area: 'LGBTQIAP+',
  Site: 'https://www.todxs.org/'
},
{
  Id: 6,
  Avatar: 'assets/ongs/Greenpeace.jpg',
  Name: 'Greenpeace',
  Nivel_Carencia: 4,
  Descricao: "Atuamos na defesa do planeta por diferentes meios, promovendo conhecimento e conscientização pública, mobilização social e articulação política para diversas causas, sempre de forma independente. Só assim podemos denunciar os crimes ambientais e confrontar governos e empresas que ameaçam o meio ambiente. Conheça o trabalho que realizamos no Brasil.",
  Area: 'Meio Ambiente',
  Site: 'https://www.greenpeace.org/'
},
{
  Id: 7,
  Avatar: 'assets/ongs/Medicos_DO_Mundo.png',
  Name: 'Medicos do Mundo',
  Nivel_Carencia: 3,
  Descricao: "Como a maioria das organizações não governamentais em seu início eram poucos voluntários, liderados pelo Dr Mário Guimarães  e muita vontade de ajudar! Eram feitos atendimentos gerais a fim de ajudar as pessoas em estado de vulnerabilidade e que se encontravam morando nas ruas de São Paulo. Aos poucos o grupo foi aumentando e se organizando e hoje já conta com diversas especialidades, em cinco estados e continua crescendo para outras cidades e estados. Ainda que com uma estrutura simples com materiais, medicamentos e equipamentos doados e guardados em sua maioria nas casas de membros da organização a Médicos do Mundo conta com um grupo extraordinário de voluntários de todas as áreas da medicina humana, e animal além de outros serviços como cuidados com higiene, em parceria com outras ONGs, serviços de atendimento legal, alimentação, pedagógicos e ainda estamos crescendo!",
  Area: 'Medica',
  Site: 'https://www.medicosdomundo.org.br/'
},
{
  Id: 8,
  Avatar: 'assets/ongs/Amor_De_Raca.jpg',
  Name: 'Amor de Raça',
  Nivel_Carencia: 1,
  Descricao: "A ONG amor de raça não tem sede física, nem local para abrigar animais.. NÃO FAZEMOS RESGASTES!!! Apenas orientação com apadrinhamento.. um resgate de animal dependendo do caso exige no mínimo $400,00. AGRADEÇO A COMPREENSÃO DE TODOS",
  Area: 'Animais',
  Site: 'https://www.facebook.com/amorderaca/'
},
{
  Id: 9,
  Avatar: 'assets/ongs/CEPFS.png',
  Name: 'CEPFS',
  Nivel_Carencia: 2,
  Descricao: "Entre os galhos retorcidos do sertão, há água. No solo árido, cresce o verde. Da sabedoria popular, nascem tecnologias sociais. Transformações como essas são possíveis no semiárido brasileiro com o trabalho desenvolvido pelo Cepfs. Premiada nacional e internacionalmente, a organização não governamental é referência no desenvolvimento rural sustentável a partir de inovações na captação de água da chuva, potencializando recursos naturais próprios da caatinga, e no empoderamento de famílias e agricultores do Nordeste.",
  Area: 'Comunidades Carentes',
  Site: 'https://cepfs.org.br/'
},
{
  Id: 10,
  Avatar: 'assets/ongs/AACD.png',
  Name: 'AACD',
  Nivel_Carencia: 4,
  Descricao: "AACD oferece mais de 5000 atendimentos todos os dias, graças ao apoio de pessoas como você. Nos ajude a melhorar a vida das pessoas com deficiência física. Sem você não existe AACD. Doe para a AACD. Doe Felicidade. Doe Amor. Teleton 2020. Doe Carinho. Doe Afeto.",
  Area: 'Pessoas Com Deficiecia',
  Site: 'https://aacd.org.br/'
}
]

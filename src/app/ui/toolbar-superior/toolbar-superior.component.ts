import { CadastroComponent } from './../cadastro/cadastro.component';
import { LoginComponent } from '../modal/login/login.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalBoasVindasComponent } from './../modal/modal-boas-vindas/modal-boas-vindas.component';

@Component({
  selector: 'app-toolbar-superior',
  templateUrl: './toolbar-superior.component.html',
  styleUrls: ['./toolbar-superior.component.scss']
})
export class ToolbarSuperiorComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  public openDialog() {
    const dialogRef = this.dialog.open(ModalBoasVindasComponent);
  }

  public openLogin() {
    const dialogRef = this.dialog.open(LoginComponent);
  }

  public openCadastro(){
    const dialogRef = this.dialog.open(CadastroComponent);
  }

  ngOnInit(): void {
  }

}

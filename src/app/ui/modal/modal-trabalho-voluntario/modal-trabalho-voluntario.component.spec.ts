import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTrabalhoVoluntarioComponent } from './modal-trabalho-voluntario.component';

describe('ModalTrabalhoVoluntarioComponent', () => {
  let component: ModalTrabalhoVoluntarioComponent;
  let fixture: ComponentFixture<ModalTrabalhoVoluntarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTrabalhoVoluntarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTrabalhoVoluntarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

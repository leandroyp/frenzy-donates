import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar) { }
  ShowSnackBar(message: string){
    this._snackBar.open(message, 'Ok');
  }


  ngOnInit(): void {
  }

}

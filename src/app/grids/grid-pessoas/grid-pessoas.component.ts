import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import {IgxGridComponent, IgxStringFilteringOperand,} from 'igniteui-angular';
import {PessoasData} from '../../data/dataPESSOAS'
import {EmpresasData} from '../../data/dataEMPRESAS'

@Component({
  selector: 'app-grid-pessoas',
  templateUrl: './grid-pessoas.component.html',
  styleUrls: ['./grid-pessoas.component.scss']
})
export class GridPessoasComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public grid1: IgxGridComponent;
  public Datasource: MatTableDataSource<DadosPessoas>;
  public displayedColumns: string[] = ['Rank', 'Name', 'Points'];
  public tipo_doador: string
  clickedRows = new Set<DadosPessoas>();
  constructor() { }

  ngOnInit(): void {

      this.tipo_doador = "Pessoa Fisica";
      this.Datasource = new MatTableDataSource<DadosPessoas>(this.CalculaRank(PessoasData.sort((a, b) => b.Points - a.Points)));
      console.log(this.Datasource)
  }

  ngAfterViewInit() {
    this.Datasource.paginator = this.paginator;
    this.Datasource.sort = this.sort;
  }

  public CalculaRank(list): any[] {

      for (let index = 0; index < list.length; index++) {
        list[index].Posicao = index + 1;
      }
      console.log(list);
      return list
  }

  public OnSelectChange(){
      console.log(this.tipo_doador);
      if(this.tipo_doador == "Pessoa Fisica"){
        this.Datasource = new MatTableDataSource<DadosPessoas>(this.CalculaRank(PessoasData.sort((a, b) => b.Points - a.Points)));
        this.Datasource.paginator = this.paginator;
        this.Datasource.sort = this.sort;
      }
      else{
        this.Datasource = new MatTableDataSource<DadosPessoas>(this.CalculaRank(EmpresasData.sort((a, b) => b.Points - a.Points)));
        this.Datasource.paginator = this.paginator;
        this.Datasource.sort = this.sort;
      }

  }

  public Top3(cell): boolean {
    const top = cell.row.index < 4;
    console.log();
    return top;
  }

  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.Datasource.filter =  filterValue.trim().toLowerCase();

    if (this.Datasource.paginator) {
      this.Datasource.paginator.firstPage();
    }
  }

}

export interface DadosPessoas {
  Id: number;
  Name: string;
  Points: number;
  Ispessoa: boolean;
  Posicao: number
}


import { ONGSData } from './../../data/dataONGS';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalDoacaoComponent } from '../modal/modal-doacao/modal-doacao.component';
import { ModalTrabalhoVoluntarioComponent } from './../modal/modal-trabalho-voluntario/modal-trabalho-voluntario.component';

@Component({
  selector: 'app-perfil-ong',
  templateUrl: './perfil-ong.component.html',
  styleUrls: ['./perfil-ong.component.scss']
})
export class PerfilOngComponent implements OnInit {
  OngName :string;
  OngObject: OngElement
  IsOng: boolean = true;
  constructor(private route: ActivatedRoute,public dialog: MatDialog) { }

  public tiles: Tile[] = [
    {cols: 1, rows: 2},
    {cols: 3, rows: 2},
    {cols: 4, rows: 2},
    {cols: 4, rows: 4},
  ];

  ngOnInit() {
    this.OngName = this.route.snapshot.paramMap.get('nome');
    this.recuperaOng(this.OngName);
  }


  public openAgendamendo(){
    const dialogRef = this.dialog.open(ModalTrabalhoVoluntarioComponent);
  }

  public openDoacao(){
    const dialogRef = this.dialog.open(ModalDoacaoComponent);
  }

  recuperaOng(ongName:String){

    ONGSData.forEach(element => {
      if (element.Name.toLowerCase().trim() == ongName.toLowerCase().trim()){
          this.IsOng = true;
          this.OngObject = <OngElement>element;
          return true;
      }
    });
    if(!this.OngObject){
      this.IsOng = false;
    }
    return
  }

}

export interface OngElement {
  Id: number;
  Avatar: string;
  Name: string;
  Nivel_Carencia: number;
  Area: string;
  Descricao:string;
}

export interface Tile {
  cols: number;
  rows: number;
}

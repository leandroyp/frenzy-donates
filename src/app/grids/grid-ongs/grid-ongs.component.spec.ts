import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridOngsComponent } from './grid-ongs.component';

describe('GridOngsComponent', () => {
  let component: GridOngsComponent;
  let fixture: ComponentFixture<GridOngsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridOngsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridOngsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

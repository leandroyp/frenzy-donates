import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar) { }
  ShowSnackBar(message: string){
    this._snackBar.open(message, 'Ok');
  }

  ngOnInit(): void {
  }

}

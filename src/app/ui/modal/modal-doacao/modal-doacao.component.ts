import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModalConfirmacaoComponent } from '../modal-confirmacao/modal-confirmacao.component';

@Component({
  selector: 'app-modal-doacao',
  templateUrl: './modal-doacao.component.html',
  styleUrls: ['./modal-doacao.component.scss']
})
export class ModalDoacaoComponent implements OnInit {

  constructor(private _snackBar: MatSnackBar,public dialogRef: MatDialogRef<ModalDoacaoComponent>,public dialog: MatDialog) { }
  ShowSnackBar(message: string){
    this._snackBar.open(message, 'Ok');
    this.dialogRef.close();
    const dialogRef = this.dialog.open(ModalConfirmacaoComponent);
    }

  ngOnInit(): void {
  }

}

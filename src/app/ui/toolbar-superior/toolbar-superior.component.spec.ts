import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarSuperiorComponent } from './toolbar-superior.component';

describe('ToolbarSuperiorComponent', () => {
  let component: ToolbarSuperiorComponent;
  let fixture: ComponentFixture<ToolbarSuperiorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolbarSuperiorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarSuperiorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, ViewChild} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {ONGSData} from '../../data/dataONGS'

/**
 * @title Table with expandable rows
 */
@Component({
  selector: 'app-grid-ongs',
  styleUrls: ['./grid-ongs.component.scss'],
  templateUrl: './grid-ongs.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class GridOngsComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public DataSource : MatTableDataSource<OngElement>;
  public Categoria: string
  public Ordenacao: string

  columnsToDisplay = ['-', 'Nome', 'Situação'];
  expandedElement: OngElement | null;

ngOnInit(): void {
    this.Categoria = "Todas";
    this.Ordenacao = "Mais Carentes"
    this.DataSource = new MatTableDataSource<OngElement>(ONGSData.sort((a, b) => a.Nivel_Carencia - b.Nivel_Carencia));
    console.log(this.DataSource)
}

public OnSelectChangeCategoria(): void{

  if(this.Categoria != 'Todas'){
    this.DataSource.filter =  this.Categoria.trim().toLowerCase();

    if (this.DataSource.paginator) {
      this.DataSource.paginator.firstPage();
    }
  }
  else{
    this.DataSource.filter = ""
    if (this.DataSource.paginator) {
      this.DataSource.paginator.firstPage();
    }
  }
}

public OnSelectChangeOrdenacao(): void{

  if(this.Ordenacao == "Mais Carentes"){
      this.DataSource = new MatTableDataSource<OngElement>(ONGSData.sort((a, b) => a.Nivel_Carencia - b.Nivel_Carencia));
      this.DataSource.paginator = this.paginator;
      this.DataSource.sort = this.sort;
      this.OnSelectChangeCategoria()
  }
  else{
      this.DataSource = new MatTableDataSource<OngElement>(ONGSData.sort((a, b) => b.Nivel_Carencia - a.Nivel_Carencia));
      this.DataSource.paginator = this.paginator;
      this.DataSource.sort = this.sort;
      this.OnSelectChangeCategoria()
  }

}

ngAfterViewInit() {
    this.DataSource.paginator = this.paginator;
    this.DataSource.sort = this.sort;
}

}

export interface OngElement {
  Id: number;
  Avatar: string;
  Name: string;
  Nivel_Carencia: number;
  Area: string;
}


  /* eslint-disable @typescript-eslint/naming-convention */

/* tslint:disable */
export const EmpresasData = [{
    Id: 61,
    Name: 'LG',
    Points: 94559300,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 62,
    Name: 'Mercado Livre',
    Points: 1843280,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 63,
    Name: 'Microsoft',
    Points: 26012100,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 64,
    Name: 'Pepsi',
    Points: 51622350,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 65,
    Name: 'Coca Cola',
    Points: 86033580,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 66,
    Name: 'Amazon',
    Points: 17618610,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 67,
    Name: 'Good & Food',
    Points: 4990712,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 68,
    Name: 'Burger King',
    Points: 7046465,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 69,
    Name: 'Subway',
    Points: 7333370,
    Ispessoa: false,
    Posicao:0
  },
  {
    Id: 70,
    Name: 'Cora',
    Points: 8156996,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 71,
    Name: 'Outstone',
    Points: 2529137,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 72,
    Name: 'Ecolig',
    Points: 2427300,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 73,
    Name: 'Tide Cleaners',
    Points: 1443168,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 74,
    Name: 'Samarco',
    Points: 3733541,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 75,
    Name: 'Smti',
    Points: 9157246,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 76,
    Name: 'Spartan',
    Points: 3380140,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 77,
    Name: 'Zubat',
    Points: 1621988,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 78,
    Name: 'Nike',
    Points: 2709752,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 79,
    Name: 'Tadeu Festas',
    Points: 5232882,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 80,
    Name: 'Naturalle',
    Points: 4950064,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 81,
    Name: 'Apple',
    Points: 3864777,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 82,
    Name: 'Aperture',
    Points: 6069552,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 83,
    Name: 'Bs2',
    Points: 5228742,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 84,
    Name: 'Keep sports',
    Points: 9011903,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 85,
    Name: 'Seenit',
    Points: 6650144,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 86,
    Name: 'Avianca',
    Points: 3715741,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 87,
    Name: 'Company',
    Points: 9457001,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 88,
    Name: 'Us Closed',
    Points: 1818023,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 89,
    Name: 'Adidas',
    Points: 560571600,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 90,
    Name: 'Starbucks',
    Points: 766304500,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 91,
    Name: 'Dullux',
    Points: 7058333,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 92,
    Name: 'Cemig',
    Points: 9867179,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 93,
    Name: 'RR Computadores',
    Points: 235774,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 94,
    Name: 'M Agrimensura',
    Points: 9326764,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 95,
    Name: 'Olvrealty',
    Points: 4655924,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 96,
    Name: 'Tech Sun',
    Points: 5560237,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 97,
    Name: 'Unilever',
    Points: 58806600,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 98,
    Name: 'Gucci',
    Points: 328402100,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 99,
    Name: 'Safyi Real Estate',
    Points: 2160033,
    Ispessoa: false,
    Posicao:0
  }
  ,
  {
    Id: 100,
    Name: 'Pro Tech',
    Points: 202793,
    Ispessoa: false,
    Posicao:0
  }
]
